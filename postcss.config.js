module.exports = {
    syntax: 'postcss-scss',
    plugins: [
        require("postcss-import")(),
        require('precss')(),
        require('postcss-preset-env')({browsers: 'last 2 versions'}), // allready have autoprefixer
      ]
}
