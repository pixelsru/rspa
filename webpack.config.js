const HtmlWebPackPlugin = require("html-webpack-plugin");
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const Ip = require("ip");

module.exports = {
  module: {
    rules: [
      {
        test: /\.(s|a|c)+ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: process.env.NODE_ENV === 'development',
            },
          },
	  'css-loader',
          'postcss-loader',
	]
      },
      {
        enforce: "pre",
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: "eslint-loader",
        options: {
          fix: true
        }
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      }
    ]
  },
  plugins: [
    new StyleLintPlugin({
       failOnError: false,
       emitWarning: false,
       emitErrors: false,
       context: './src/css',
       files: '**/*.css',
       fix: true,
    }),
    new FriendlyErrorsWebpackPlugin({
      compilationSuccessInfo: {
        messages: [`┬┴┬┴┤･ω･)ﾉ http://${Ip.address()}:4000 ┬┴┬┴┤(･_├┬┴┬┴`],
        notes: ['Some additionnal notes to be displayed unpon successful compilation']
      }
    }),
    new MiniCssExtractPlugin(),
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    })
  ],
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  devServer: {
    host: '0.0.0.0', // to serve not only localhost
    port: 4000,
    quiet: true, // remove default errors
  },
};
